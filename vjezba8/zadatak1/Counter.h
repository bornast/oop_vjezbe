#pragma once
#include "Videogame.h"

namespace OSS {

	class Counter
	{
	private:
		int _xbox;
		int _ps4;
		int _pc;


	public:
		Counter();
		~Counter();
		string mostCommon();
		void platformCounter(VideoGame* &videoGame);
	};
}