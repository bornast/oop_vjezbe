#pragma once
#include "Action.h"

namespace OSS {
	class LastOfUs2 :
		public Action
	{
	private:
		vector<string> _platforms;
	public:
		LastOfUs2();
		~LastOfUs2();
		virtual vector<string> platforms();
		void addPlatform(string str);
		vector<string> platforms();
	};
}
