#include "pch.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "Videogame.h"
#include "Action.h"
#include "Godofwar.h"
#include "Witcher3.h"
#include "Darksouls.h"
#include "Fallout4.h"
#include "LastOfUs2.h"
#include "Counter.h"


using namespace std;
using namespace OSS;


vector<string> getValidGames(string str)
{
	vector<string> validGames;
	if (str.find("PC") != string::npos)
		validGames.push_back("PC");
	if (str.find("XBOX") != string::npos)
		validGames.push_back("XBOX");
	if (str.find("PS4") != string::npos)
		validGames.push_back("PS4");

	return validGames;

}

int main(void)
{
	
	VideoGame* games[] = { new Witcher3, new DarkSouls, new Fallout4, new LastOfUs2, new GodOfWar };
	ifstream list("list.txt");
	string str;
	Counter counter;
	
	while (getline(list, str)) {

		vector<string> validGames = getValidGames(str);
		if (validGames.size() > 0)
		{
			string gameName = str.substr(0, str.find('#'));
			if (gameName == "Witcher3") {
				for (int i = 0; i < validGames.size(); i++)
					games[0]->addPlatform(validGames[i]);
			}
			else if (gameName == "DarkSouls") {
				for (int i = 0; i < validGames.size(); i++)
					games[1]->addPlatform(validGames[i]);
			}
			else if (gameName == "Fallout4") {
				for (int i = 0; i < validGames.size(); i++)
					games[2]->addPlatform(validGames[i]);
			}
			else if (gameName == "LastOfUs2") {
				for (int i = 0; i < validGames.size(); i++)
					games[3]->addPlatform(validGames[i]);
			}
			else if (gameName == "GodOfWar") {
				for (int i = 0; i < validGames.size(); i++)
					games[4]->addPlatform(validGames[i]);
			}
		}
	}

	for (int i = 0; i < 5; i++)
		counter.platformCounter(games[i]);
	
	cout <<  "najzastupljenija platforma je " << counter.mostCommon() << endl;
}

