#pragma once
#include "VideoGame.h"

namespace OSS {
	class Rpg :
		virtual public VideoGame
	{
	protected:
		string type = "RPG";
	public:
		Rpg();
		~Rpg();
		string getType();
		virtual void addPlatform(string str) {};
		//virtual vector<string> platforms();
	};
}