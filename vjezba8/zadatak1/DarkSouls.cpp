#include "pch.h"
#include "DarkSouls.h"

using namespace OSS;

DarkSouls::DarkSouls()
{
}


DarkSouls::~DarkSouls()
{
}

vector<string> DarkSouls::platforms()
{
	return _platforms;
}

void DarkSouls::addPlatform(string platform)
{
	_platforms.push_back(platform);
}