#pragma once
#include "Rpg.h"

namespace OSS {
	class DarkSouls :
		public Rpg
	{
	private:
		vector<string> _platforms;
	public:
		DarkSouls();
		~DarkSouls();
		vector<string> platforms();
		void addPlatform(string str);
	};
}