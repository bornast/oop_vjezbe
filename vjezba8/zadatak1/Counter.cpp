#include "pch.h"
#include "Counter.h"

using namespace OSS;

Counter::Counter()
{
}


Counter::~Counter()
{
}

void Counter::platformCounter(VideoGame* &game)
{
	vector <string> platforms = game->platforms();
	
	for (int i = 0; i < platforms.size(); i++) {

		if (platforms[i] == "PC") _pc += 1;
		else if (platforms[i] == "PS4") _ps4 += 1;
		else if (platforms[i] == "XBOX") _xbox += 1;
	}
}

string Counter::mostCommon() {

	if (_pc > _ps4 && _pc > _xbox) return "PC";
	else if (_pc < _ps4 && _ps4 > _xbox) return "PS4";
	else if (_pc < _xbox && _ps4 < _xbox) return "XBOX";
	
}
