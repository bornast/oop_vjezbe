#pragma once
#include "Action.h"

namespace OSS {
	class GodOfWar :
		public Action
	{
	private:
		vector<string> _platforms;
	public:
		GodOfWar();
		~GodOfWar();
		vector<string> platforms();
		void addPlatform(string str);
	};
}
