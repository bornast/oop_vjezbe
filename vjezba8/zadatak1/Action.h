#pragma once
#include "VideoGame.h"

namespace OSS {
	class Action :
		virtual public VideoGame
	{
	protected:
		string type = "RPG";
	public:
		Action();
		~Action();
		string getType();
		virtual void addPlatform(string str) {};
		//virtual vector<string> platforms() { };

	};
}