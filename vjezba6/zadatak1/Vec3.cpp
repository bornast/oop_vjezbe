#pragma once
#include "pch.h"
#include <string>
#include "Vec3.h"

using namespace std;
using namespace oop;


int Vec3::_counter = 0;

float Vec3::getX()
{
	return _x;
}

float Vec3::getY()
{
	return _y;
}

float Vec3::getZ()
{
	return _z;
}

Vec3::Vec3(float x, float y, float z)
{
	_x = x;
	_y = y;
	_z = z;
	_counter++;
}

Vec3 Vec3::operator+(const Vec3& other) 
{
	return Vec3((_x + other._x), (_y + other._y), (_z + other._z));
}

Vec3 Vec3::operator-(const Vec3& other)
{
	return Vec3((_x - other._x), (_y - other._y), (_z - other._z));
}

Vec3 Vec3::operator*(int a)
{
	return Vec3((_x * a), (_y * a), (_z * a));
}

Vec3 Vec3::operator/(int a)
{
	return Vec3((_x / a), (_y / a), (_z / a));
}

Vec3& Vec3::operator+=(const Vec3& other)
{
	_x = _x + other._x;
	_y = _y + other._y;
	_z = _z + other._z;
	return *this;
}

Vec3& Vec3::operator-=(const Vec3& other)
{
	_y = _y - other._y;
	_z = _z - other._z;
	_x = _x - other._x;
	return *this;
}

Vec3& Vec3::operator*=(const Vec3& other)
{
	_y = _y * other._y;
	_z = _z * other._z;
	_x = _x * other._x;
	return *this;
}
Vec3& Vec3::operator/=(const Vec3& other)
{
	_y = _y / other._y;
	_z = _z / other._z;
	_x = _x / other._x;
	return *this;
}

bool Vec3::operator==(const Vec3& other)
{
	return (_x == other._x && _y == other._y && _z == other._z);
}

bool Vec3::operator!=(const Vec3& other)
{
	return (_x != other._x && _y != other._y && _z != other._z);
}

float& Vec3::operator[](int index)
{
	if (index == 0)
		return _x;
	else if (index == 1)
		return _y;
	else if (index == 2)
		return _z;
}

float Vec3::operator*(const Vec3& other)
{
	return _x * other._x + _y * other._y + _z * other._z;
}

void Vec3::normalizacija() {
	_x = _x / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
	_y = _y / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
	_z = _z / (sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));

}

int Vec3::getCounter()
{
	return _counter;
}