#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void printArray(int *arr, int len) {

	for (int i = 0; i < len; i++)
		cout << arr[i] << endl;
}

struct myVector {

	int *arr;
	int len = 0;
	int elementsCount = 0;

	void vector_new(int newLen) {

		if (len > 0)
			cout << " Vector is already initialized " << endl;
		else {
			arr = new int[newLen];
			len = newLen;
		}

	}

	void vector_delete() {

		delete[] arr;
		arr = NULL;
		len = 0;
		elementsCount = 0;

	}

	void vector_push_back(int value) {

		if (len == elementsCount + 1) {

			len = len * 2;
			arr = (int*)realloc(arr, len * sizeof(int));

		}

		arr[elementsCount] = value;
		elementsCount++;

	}

	void vector_pop_back() {

		arr[elementsCount - 1] = NULL;
		elementsCount--;
	}

	int& vector_front() {

		return arr[0];
	}

	int& vector_back() {

		return arr[elementsCount - 1];

	}

	int vector_size() {

		return elementsCount;
	}

};

int main()
{
	myVector test;
	test.vector_new(3);
	test.vector_push_back(372);
	test.vector_push_back(2);
	test.vector_push_back(3);
	cout << test.len << endl;
	cout << "front je " << test.vector_front() << endl;
	cout << "back je " << test.vector_back() << endl;
	cout << "size je " << test.vector_size() << endl;
	//test.vector_delete();
	//cout << "novi size je " << test.vector_size() << endl;
	test.vector_pop_back();
	cout << "novi size je " << test.vector_size() << endl;
	printArray(test.arr, test.vector_size());
}
