#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void printArray(int *arr, int len) {

	for (int i = 0; i < len; i++)
		cout << arr[i] << endl;
}

int extractIntValueFromInput(string message) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good())
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

void sortArray(int *arr, int len) {
	
	int swap;
	for (int i = 0; i < len-1; i++) {
		for (int j = i + 1; j < len; j++) {

			if (arr[i] > arr[j]) {
				swap = arr[i];
				arr[i] = arr[j];
				arr[j] = swap;
			}
		}

	}

}

bool inArray(int *arr, int len, int num) {

	for (int i = 0; i < len; i++) {
		if (arr[i] == num) return true;
	}

	return false;
}

void fillMissingValues(int *arr, int len, int *validValuesArr, int validValuesLen) {
		
	int missingNumbers = 0;
	
	for (int i = 0; i < validValuesLen; i++) {
		if (inArray(arr, len, validValuesArr[i]) == false) {
			
			missingNumbers++;
			arr = (int*)realloc(arr, (missingNumbers + len) * sizeof(int));
			arr[len + missingNumbers - 1] = validValuesArr[i];
		}
	}

	sortArray(arr, missingNumbers + len);
	printArray(arr, missingNumbers + len);
}

int main()
{
	
	int len = extractIntValueFromInput("Unesite duzinu niza");
	int *arr = new int[len];
	int i = 0;

	while (i < len) {
		
		int number = extractIntValueFromInput("Unesite broj u rasponu od 1 do 9");
		while (number < 1 || number > 9) {
			number = extractIntValueFromInput("Unesite broj u rasponu od 1 do 9");
		}

		arr[i] = number;
		i++;
	}
	
	int validValuesArr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 15, 17 };
	int validValuesLen = sizeof(validValuesArr) / sizeof(validValuesArr[0]);

	fillMissingValues(arr, len, validValuesArr, validValuesLen);

}

