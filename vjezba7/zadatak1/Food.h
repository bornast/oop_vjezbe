#pragma once
#include <string>


using namespace std;

namespace oop {

	class Potrosnja
	{

	private:
		int _year;
		int _month;
		float _kg;

	public:
		Potrosnja();
		Potrosnja(int year, int month, float kg);
		const int getYear();
		const int getMonth();
		const float getKg();
	};

	class Food
	{
	protected:
		string _naziv;
		string _vrsta;
		float _kolicinaVoda;
		float _protein;
		float _mast;
		float _ugljikohidrat;
		string _rokTrajanja;
		float _dnevnaKolicinaHrana;
		Potrosnja *_potrosnjaHrane;
	private:
		const int toAllocate(string date);
		int _currentIndex;
		virtual void print(std::ostream& os) const {}
	public:
		Food();
		Food(string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		Food(const Food&);
		~Food();
		void uvecajPotrosnju();
		void smanjiPotrosnju();
		void dodajPodatkeOPotrosnji(Potrosnja potrosnja);
		bool currentYear(int year);
		bool consuptionExists(int year, int month);
		const int getCurrentYear();
		const float consuptionRaisedDecreased();
		const void printFoodDetails();
		void regulateDailyConsuption();
		float getDnevnaPotrosnja() const;
		friend std::ostream & operator<<(std::ostream &os, const Food& f) {
			f.print(os);
			return os;
		}
	};
}



