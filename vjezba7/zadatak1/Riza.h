#pragma once
#include "Vege.h"

namespace oop {
	class Riza :
		public Vege
	{
	public:
		Riza(float potrosnjaSamostalnogJela, float potrosnjaKaoPrilogUzJelo, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Riza();
	};
}
