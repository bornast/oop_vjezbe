#pragma once
#include "Kolaci.h"

namespace oop {

	class Madjarica :
		public Kolaci
	{
	public:
		Madjarica(float potrosnjaKaoDesert, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Madjarica();
	};
}
