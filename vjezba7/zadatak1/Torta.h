#pragma once
#include "Kolaci.h"

namespace oop {

	class Torta :
		public Kolaci
	{
	public:
		Torta(float potrosnjaKaoDesert, string vrsta, string naziv, float kolicinaVoda, float protein, float mast, float ugljikohidrat, string rokTrajnja, float dnevnaKolicinaHrane);
		~Torta();
	};

}