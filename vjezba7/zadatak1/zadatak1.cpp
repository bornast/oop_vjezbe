#include "pch.h"
#include <iostream>
#include"food.h"
#include<vector>
#include "Prsut.h"
#include "Mesni.h"
#include "Meso.h"
#include "Sunka.h"
#include "Mlijecni.h"
#include "Jogurt.h"
#include "Mlijeko.h"
#include "Sir.h"
#include "Vege.h"
#include "Hummus.h"
#include "Riza.h"
#include "Tofu.h"
#include "Kolaci.h"
#include "Krempita.h"
#include "Madjarica.h"
#include "Torta.h"


using namespace oop;
using namespace std;

int main()
{
	vector<Food*> foods;
	Prsut p(1, 40, "Mesni", "Prsut1", 1, 2, 3, 4, "01-01-2020", 1);
	foods.push_back(&p);
	Meso m(2, 50, "Mesni", "Prsut2", 1, 2, 3, 4, "01-01-2020", 2);
	foods.push_back(&m);
	Sunka s(3, 60, "Mesni", "Prsut3", 1, 2, 3, 4, "01-01-2020", 3);
	foods.push_back(&s);
	Jogurt j(4, 70, "Mlijecni", "Jogurt1", 1, 2, 3, 4, "01-01-2020", 4);
	foods.push_back(&j);
	Sir si(5, 80, "Mlijecni", "Jogurt2", 1, 2, 3, 4, "01-01-2020", 5);
	foods.push_back(&si);
	Mlijeko ml(90, 5, "Mlijecni", "Jogurt3", 1, 2, 3, 4, "01-01-2020", 6);
	foods.push_back(&ml);
	Krempita kr(100, "Kolaci", "Kolac1", 1, 2, 3, 4, "01-01-2020", 7);
	foods.push_back(&kr);
	Madjarica ma(110, "Kolaci", "Kolac2", 1, 2, 3, 4, "01-01-2020", 8);
	foods.push_back(&ma);
	Torta to(140, "Kolaci", "Kolac3", 1, 2, 3, 4, "01-01-2020", 11);
	foods.push_back(&to);
	Riza r(6, 120, "Vege", "Vege1", 1, 2, 3, 4, "01-01-2020", 9);
	foods.push_back(&r);	
	Tofu t(7, 130, "Vege", "Vege2", 1, 2, 3, 4, "01-01-2020", 10);
	foods.push_back(&t);
	Hummus h(8, 150, "Vege", "Vege3", 1, 2, 3, 4, "01-01-2020", 12);
	foods.push_back(&h);
	
	vector<Food*>::iterator it;
	float total = 0.0;
	
	for (int i = 0; i < foods.size(); i++) {
		total = total + foods[i]->getDnevnaPotrosnja();
		cout << *foods[i] << endl;
	}

	cout << "ukupna potrosnja: " << total << endl;
}
