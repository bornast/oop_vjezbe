#include "pch.h"
#include "Target.h"
#include <iostream>
#include <math.h>

using namespace std;

Target::Target(Point point, double height)
{
	_point = point;
	_height = height;
	_hit = false;
}

const Point Target::getPoint() {
	return _point;
}

const double Target::getHeight() {
	return _height;
}

void Target::hit() {
	_hit = true;
}
