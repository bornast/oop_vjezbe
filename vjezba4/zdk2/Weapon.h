#pragma once
#ifndef WEAPON_H
#define WEAPON_H

#include "../vjezba4/Point.h";


class Weapon
{

private:
	Point _point;
	int _numberOfBullets;
	int _bulletsLeft;

public:
	void shoot();
	void reload();
	Weapon(Point point, int numberOfBullets = 7);

	const Point getPoint();

};

#endif