#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;


string formatPunctuationSigns(string str) {


	for (int i = 0; i < str.length(); i++) {

		int continuousWhitespaces = 0;

		if (ispunct(str[i])) {

			for (int j = i - 1; j >= 0; j--) {

				if (str[j] == ' ') continuousWhitespaces++;
				else {
					continuousWhitespaces = continuousWhitespaces > 0 ? continuousWhitespaces : 0;
					break;
				}

			}

			if (continuousWhitespaces > 0) {
				str.erase(i - continuousWhitespaces, continuousWhitespaces);
				/*i -= continuousWhitespaces -1;*/
			}

		}

	}

	return str;

}

int main()
{
	string str = "Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .";
	//Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.
	cout << formatPunctuationSigns(str) << endl;

}