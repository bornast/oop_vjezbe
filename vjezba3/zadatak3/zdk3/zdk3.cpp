#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

string formatPunctuationSigns(string str) {

	for (int i = 0; i < str.length(); i++) {

		int continuousWhitespaces = 0;

		if (ispunct(str[i])) {

			for (int j = i - 1; j >= 0; j--) {

				if (str[j] == ' ') continuousWhitespaces++;
				else {
					continuousWhitespaces = continuousWhitespaces > 0 ? continuousWhitespaces : 0;
					break;
				}

			}

			if (continuousWhitespaces > 0) {
				str.erase(i - continuousWhitespaces, continuousWhitespaces);
				i -= continuousWhitespaces;
				
			}

			if (i + 1 < str.length() - 1 && str[i + 1] != ' ') {
				string replaceWith = "";
				replaceWith += str[i];
				replaceWith += ' ';				
				str.replace(i, 1, replaceWith);
			}
		}

	}

	return str;

}

int main()
{
	string str = "Ja bih ,ako ikako mogu , ovu recenicu napisala ispravno .";
	//Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.
	cout << formatPunctuationSigns(str) << endl;

}