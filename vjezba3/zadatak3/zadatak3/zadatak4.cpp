#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

vector<int> extractedMatches;

int extractIntValueFromInput(string message, int min, int max) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good() || (returnValue < min || returnValue > max))
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "error, " << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

int generateRandomNumber(int maxValue) {

	return (rand() % maxValue) + 1;
}

int getMatchesLeft() {

	int total = 0;
	vector<int>::iterator it;

	for (it = extractedMatches.begin(); it < extractedMatches.end(); it++)
		total += *it;

	return 21 - total;
}

bool gameOver() {

	int matchesLeft = getMatchesLeft();
	if (matchesLeft <= 1) return true;

	return false;
}

void gameStart() {

	while (true) {

		int computerTakes = generateRandomNumber(3);
		cout << "computer takes " << computerTakes << " matches" << endl;
		extractedMatches.push_back(computerTakes);
		cout << "matches left: " << getMatchesLeft() << endl << endl;

		if (gameOver()) {
			cout << "computer won" << endl;
			break;
		}

		int playerTakes = extractIntValueFromInput("take matches", 1, 3);
		cout << "player takes " << playerTakes << " matches" << endl;
		extractedMatches.push_back(playerTakes);
		cout << "matches left: " << getMatchesLeft() << endl << endl;
		if (gameOver()) {
			cout << "player won" << endl;
			break;
		}


	}

}

int main()
{
	srand(time(0));

	gameStart();

}
