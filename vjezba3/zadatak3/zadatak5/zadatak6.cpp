#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

using namespace std;

vector<string> strings;

int extractIntValueFromInput(string message) {

	int returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!cin.good())
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "gre�ka, " << message << endl;
		cin >> returnValue;

	}

	return returnValue;

}

string transformString(string str) {

	for (int i = 0; i < str.length(); i++) {

		string replaceWith = "";
		int continuousChar = 1;

		if (isdigit(str[i]) && i + 1 < str.length() && !isdigit(str[i + 1])) {

			for (int j = 0; j < str[i] - '0'; j++)
				replaceWith += str[i + 1];

			str.replace(i, 2, replaceWith);
			i += replaceWith.length() - 1;
		}

		else if (!isdigit(str[i])) {

			for (int j = i + 1; j < str.length(); j++) {
				if (str[j] == str[i]) continuousChar++;
				else {
					continuousChar = continuousChar > 1 ? continuousChar : 1;
					break;
				}
			}

			if (continuousChar > 1) {
				str.replace(i, continuousChar, to_string(continuousChar) + str[i]);
				i += 1;
			}

		}
	}

	return str;
}

bool stringValid(string str) {

	for (int i = 0; i < str.length(); i++) {

		if (!isupper(str[i]) && !isdigit(str[i]))
			return false;

	}

	return true;

}

string extractStringValueFromInput(string message) {

	string returnValue;

	cout << message << endl;
	cin >> returnValue;
	while (!stringValid(returnValue))
	{
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "error, " << message << endl;
		cin >> returnValue;

	}

	return transformString(returnValue);

}

void printVectorValues() {

	for (int i = 0; i < strings.size(); i++) {
		cout << strings[i] << endl;;
	}

}

int main()
{

	int stringsToAdd = extractIntValueFromInput("enter number of strings to add!");

	while (stringsToAdd > 0) {

		strings.push_back(extractStringValueFromInput("enter a string that contains only upper letters and numbers"));
		stringsToAdd--;

	}

	printVectorValues();

}
