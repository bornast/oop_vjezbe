#include "pch.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <math.h> 

using namespace std;


void printArrayDuplicates(int *arr, int n) {

	//O(N2)
	for (int i = 0; i < n - 1; i++) {		
		
		//nemoj ic kroz vec usporedene
		if (arr[i] >= 0) {
			
			int count = 1;

			for (int j = i+1; j < n; j++) {

				if (arr[i] == arr[j]) {
					count++;
					arr[j] = arr[j] * (-1); //znaci da smo ga usporedili
				}
			}

			if (count > 1) {
				std::cout << "broj " << arr[i] << " se ponavlja " << count << " puta " << std::endl;
			}

		}
		

	}

	for (int i = 0; i < n; i++) {
		if (arr[i] < 0) {
			arr[i] = arr[i] * (-1);
		}
	}
}

int main()
{
	//**********************zadatak i/o***********************
	bool flag;
	std::cout << "unesi " << true << " - " << false << std::endl;
	std::cin >> flag;
	std::cout << std::boolalpha << flag << std::endl;
	int a = 255;
	std::cout << "hex " << std::hex << a << std::endl;
	std::cout << "dec " << std::dec << a << std::endl;
	std::cout << "oct " << std::oct << a << std::endl;
	double pi = 3.141592;
	std::cout << "pi = " << std::scientific << std::uppercase;
	std::cout << std::setprecision(7) << std::setw(18) << std::setfill('0');
	std::cout << pi << std::endl;


	//**********************zadatak 1**********************
	int arr[] = { 1, 5, 1, 1, 6, 7, 5 };
	int n = sizeof(arr)/sizeof(arr[0]);
	printArrayDuplicates(arr, n);
	

}

