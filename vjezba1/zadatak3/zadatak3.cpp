#include "pch.h"
#include <iostream>

using namespace std;


int& sumOnesAndThousands(int arr[], int len) {

	for (int i = 0; i < len; i++) {

		int thousand = floor((arr[i] % 10000) / 1000);
		int one = arr[i] % 10;

		if (thousand + one == 5) return arr[i];
	}

}

int main()
{
	int arr[] = { 1123818 , 123128 , 1239, 2003 };
	int len = sizeof(arr) / sizeof(arr[0]);
	sumOnesAndThousands(arr, len) += 1;

	for (int i = 0; i < len; i++) {
		cout << arr[i] << endl;
	}
}


